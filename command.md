Handy MySQL Commands
List all databases on the sql server.	show databases;
Switch to a database.	use [db name];
To see all the tables in the db.	show tables;
To check all field in table. describe table_name;


```
knex init
knex migrate:create add_venue
knex migrate:up

knex seed:make venue

npm install apollo-server graphql
```

Homework
--------
+ inventory
+++ id
+++ venue_id
+++ name
+++ description

Mutation Example Query // Working
mutation{
  updateInventory(id:100, data:{
    name: "Rottnak"
    description: "Check on update"
  })
}

Query {
  inventory(id: Int): Inventory
  inventoryList(venueID: Int): [Inventory]
}

type Inventory {
    ....
}