async function InventoryListResolver(_, {venue_id}, ctx) {
  //if (!ctx.user) throw "You do not have a valid token";
  //if (!venue_id) throw "You do not provide Which Venue to list its Inventory";
  //const if_venu_exist_on_user = await ctx.knex.where({ user_id: ctx.user.id, id:venue_id }).select(['id','name','user_id']).from('venue').first();

  //console.log(if_venu_exist_on_user);
  //if ( if_venu_exist_on_user === undefined ) throw "You do not have permission to access this venue";
  const result = await ctx.knex.where({venue_id: venue_id}).select([ 'id', 'venue_id', 'name', 'description' ]).from('Inventory');
  return result;
}

async function CreateInventoryResolver(_, {venue_id, data }, ctx) {
  // HOMEWORK
  // - Make sure that user can only create inventory of venue of that user
  if (!ctx.user) throw "You do not have permission to create new inventory for this venue";
  if (!venue_id) throw "You do not provide Which Venue to list its Inventory";
  const if_venu_exist_on_user = await ctx.knex.where({ user_id: ctx.user.id, id:venue_id }).select(['id','name','user_id']).from('venue').first();
  //console.log(if_venu_exist_on_user);
  if ( if_venu_exist_on_user === undefined ) throw "You can not add new inventory to venue you do not own";
  const result = await ctx.knex('inventory').insert({
    ...data,
    venue_id: venue_id
  });
  return result[0];
  //return if_venu_exist_on_user;
}

async function InventoryResolver(_, { id }, ctx) {
  const result = await ctx.knex('inventory').where({ id }).select([ 'id', 'venue_id', 'name', 'description' ]);
  if (result.length > 0) return result[0];
  return null;
}

async function UpdateInventoryResolver(_, { id,venue_id, data }, ctx) {
  // HOMEWORK
  // - Make sure that user can only update inventory of venue of that user
  const if_venu_exist_on_user = await ctx.knex.where({ user_id: ctx.user.id, id:venue_id }).select(['id','name','user_id']).from('venue').first();
  //console.log(if_venu_exist_on_user);
  if ( if_venu_exist_on_user === undefined ) throw "You can not update inventory of venue you do not own";
  await ctx.knex('inventory').where({ id }).update(data);
  return true;
}

module.exports = {
  Query: {
    inventory: InventoryResolver,
    inventoryList: InventoryListResolver
  },

  Mutation: {
    updateInventory: UpdateInventoryResolver,
    createInventory: CreateInventoryResolver
  }
};