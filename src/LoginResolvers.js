const sha1 = require('sha1');
const crypto = require('crypto');

async function LoginResolver(_, { username, password }, ctx) {
  const user = await ctx.knex('user').where({ username }).first();

  if (user) {
    if (user.password === sha1(password)) {
      const token = crypto.randomBytes(48).toString('hex');

      await ctx.knex('user_login_token').insert({
        user_id: user.id,
        token,
        created_at: new Date(),
      });

      return token;
    }
  }
  return null;
}

async function MeResolver(_, __, ctx) {
  if (ctx.user) {
    return {
      id: ctx.user.id,
      username: ctx.user.username,
    }
  }
  return null;
}


module.exports = {
  Query: {
    me: MeResolver,
  },

  Mutation: {
    login: LoginResolver,
  }
}