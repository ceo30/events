const { ApolloServer, gql } = require('apollo-server');
const setting = require('./../knexfile.js');
const venueResolvers = require('./VenueResolvers');
const inventoryResolvers = require('./InventoryResolvers');
const loginResolvers = require('./LoginResolvers');
const knex = require('knex')(setting.development);
const fs = require('fs');

// The GraphQL Schema
const schema = fs.readFileSync('./src/schema.gql');
const typeDefs = gql`${schema}`;

// A map of functions which return data for the schema.
const resolvers = [
  venueResolvers,
  inventoryResolvers,
  loginResolvers
];

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    let user = undefined;

    if (req.query.token) {

      const token = await knex('user_login_token').where({ token: req.query.token }).first();

      if (token) {
        const token_user = await knex('user').where({ id: token.user_id }).first();
        if (token_user) {
          user = token_user;
        }
      }
    }
    return {
      user,
      knex: knex
    };
  }

});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`)
});