async function VenueListResolver(_, { userID }, ctx) {
  //List Only venue that associated with specific user
  // if (!ctx.user) throw "You need a valid token to view venue";
  let where = {};
  if (userID) where = { ...where, user_id: userID };

  const result = await ctx.knex.where(where).select([ 'id', 'name', 'description' ]).from('venue');
  return result;
}

async function CreateVenueResolver(_, { data }, ctx) {
  if (!ctx.user) throw "You do not have permission to create new venue";
  const result = await ctx.knex('venue').insert({
    ...data,
    user_id: ctx.user.id
  });

  return result[0];
}

async function VenueResolver(_, { id }, ctx) {
  const result = await ctx.knex('venue').where({ id }).select([ 'id', 'name', 'user_id' ]);
  if (result.length > 0) return result[0];
  return null;
}

async function UpdateVenueResolver(_, { id, data }, ctx) {
  // HOMEWORK
  // - Make sure that user can only edit its own venue
  const if_venu_exist_on_user = await ctx.knex.where({ user_id: ctx.user.id, id:id }).select(['id','name','user_id']).from('venue').first();
  //console.log(if_venu_exist_on_user);
  if ( if_venu_exist_on_user === undefined ) throw "You can not add new inventory to venue you do not own";
  if (!ctx.user) throw "You do not have permission to update this venu";
  await ctx.knex('venue').where({ id }).update(data);
  return true;
}

module.exports = {
  Query: {
    venue: VenueResolver,
    venueList: VenueListResolver
  },

  Mutation: {
    updateVenue: UpdateVenueResolver,
    createVenue: CreateVenueResolver
  }
};