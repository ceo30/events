const faker = require('faker');

function createFakeVenue(id) {
  return {
    id,
    name: faker.name.findName(),
    description: faker.lorem.text(),
    user_id: (Math.random() * 3) | 0
  }
}

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('venue').del()
    .then(function () {
      const venues = [];

      for(let i = 1; i < 100; i++) {
        venues.push(createFakeVenue(i));
      }
      // Inserts seed entries
      return knex('venue').insert(venues);
    });
};
