const sha1 = require('sha1');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('user').del()
    .then(function () {
      // Inserts seed entries
      return knex('user').insert([
        { id: 1, username: 'nak', password: sha1('123456') }
      ]);
    });
};
