const faker = require('faker');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('inventory').del()
    .then(function () {
      // Inserts seed entries
      const invetory = [];

      for(let i = 0; i < 100; i++) {
        invetory.push(createFakeValue());
      }
      return knex('inventory').insert(invetory);
    });
};

function createFakeValue(){
  return {
    venue_id: faker.random.number(200),
    name: faker.name.findName(),
    description: faker.lorem.text()
  }
}
