
exports.up = function(knex) {
    return knex.schema.createTable('inventory', function(table) {
        table.increments();
        table.integer('venue_id');
        table.string('name');
        table.text('description');
        table.timestamps();
        });
};

exports.down = function(knex) {
  
};
