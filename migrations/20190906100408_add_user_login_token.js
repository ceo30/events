
exports.up = function(knex) {
  return knex.schema.createTable('user_login_token', function(table) {
    table.string('token');
    table.integer('user_id');
    table.primary([ 'token' ]);
    table.datetime('created_at');
  });
};

exports.down = function(knex) {
  
};
